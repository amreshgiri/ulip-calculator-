# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.test import TestCase
from calc.models import target_amount_tbl
import requests


# Create your tests here.


class test_target_amount_tbl(TestCase):

    def test_target(self):
        self.target_amount_tbl(2,10,2,end)

    def target_amount_tbl(self,a,b,c):
        u = "http://127.0.0.1:901/calc/target_amount/{}/{}/{}/".format(a, b, c)
        resp = requests.get(u).json

        self.assertEquals(resp.text,"6.72222222222")
