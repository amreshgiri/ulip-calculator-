from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^target_amount/(.+)/(.+)/(.+)/$', views.target_amount, name='target_amount'),
    url(r'^target_amount_child/(\d+)/(\d+)/(\d+)/(\d+)/$', views.target_amount_child, name='target_amount'),
    url(r'^target_amount_retirement/(\d+)/(\d+)/(\d+)/(\d+)/$', views.target_amount_retirement, name='target_amount'),
    url(r'^target_amount_investment_monthly/(\d+)/(\d+)/(\d+)/$', views.target_amount_investment_monthly, name='target_amount'),
    url(r'^target_amount_investment_annual/(\d+)/(\d+)/(\d+)/$', views.target_amount_investment_annual, name='target_amount'),
    url(r'^display_table/', views.display_table, name='display_table')

]