# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


# Create your models here.
class target_amount_tbl(models.Model):
    current_goal_amount = models.IntegerField()
    inflation_rate = models.FloatField()
    period_of_investment = models.IntegerField()
    t_amount = models.IntegerField()

    def __str__(self):
        return "%d" % (self.id)
